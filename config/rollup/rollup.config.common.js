import fs from 'fs'
import path from 'path'
import minimatch from 'minimatch'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import babel from 'rollup-plugin-babel'

const ROOT_RESOLVE = path.resolve()
const PACKAGES_RESOLVE = path.resolve('packages')
export const INCLUDES = process.env.PACKAGES ? process.env.PACKAGES.split(/\s*,\s*/) : []

const configTemplate = () => ({
  input: path.resolve(ROOT_RESOLVE, 'src', 'index.js'),
  plugins: [
    resolve(),
    commonjs({
      include: 'node_modules/**',
    }),
    babel({
      exclude: 'node_modules/**',
    }),
  ],
  external: [
    'react',
    'react-dom',
    'react-is',
    'styled-components',
  ],
})

const packageNames = fs.readdirSync(PACKAGES_RESOLVE)

// @TODO: Convert to use Ramda's `map()` function
export const configs = Object.entries(
  (packageNames).reduce((collection, name) => {
    const pathname = path.resolve(PACKAGES_RESOLVE, name)
    const isDir = fs.statSync(pathname).isDirectory()
    const isPattern = INCLUDES.some((pattern) => minimatch(pathname, `${PACKAGES_RESOLVE}/${pattern}`))

    if (isDir && isPattern) {
      return {
        ...collection,
        [pathname]: {
          ...configTemplate(),
          input: path.resolve(PACKAGES_RESOLVE, name, 'src', 'index.js'),
        },
      }
    }

    return collection
  }, {}),
)
