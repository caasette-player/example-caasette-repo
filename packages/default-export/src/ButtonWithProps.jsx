import React from 'react'
import PropTypes from 'prop-types'

export default function ButtonWithProps(props){
  return (
    <button onClick={() => buttonClick(props.border)}>
      Button
    </button>
  )
}

const buttonClick = border => {
    alert(border)
}


ButtonWithProps.propTypes = {
  /** Description for prop of ButtonWithProps */
  border: PropTypes.string,
}