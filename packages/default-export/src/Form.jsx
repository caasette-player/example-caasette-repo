import React, { Component } from 'react'
import PropTypes from 'prop-types'

const questions = [
  {
    title: "The document isn't loading",
    value: 'docNotLoading',
  },
  {
    title: 'The text is too small',
    value: 'textTooSmall',
  },
  {
    title: 'My information is wrong',
    value: 'informationWrong',
  },
  {
    title: "I can't find where to sign",
    value: 'findWhereToSign',
  },
  {
    title: "Other",
    value: 'other',
  }
]

class EsignFeedbackForm extends Component {
  constructor(props) {
    super(props)
    const initialResponses = {}
    questions.map((question) => { initialResponses[question.value] = false })

    this.state = {
      responses: initialResponses,
      feedbackText: '',
    }
  }

  get selectedResponses() {
    return questions.map((question) => {
      if (this.state.responses[question.value]) {
        return question.value
      }
    }).filter(value => !!value)
  }

  get isOtherSelected() {
    return this.state.responses.other === true
  }

  handleCheckboxChange(value) {
    this.setState((prevState) => {
      prevState.responses[value] = !prevState.responses[value]
      return prevState
    })
  }

  handleTextAreaChange(event) {
    this.setState({ feedbackText: event.target.value })
  }

  submit() {
    alert(`Feedback Text: ${this.state.feedbackText} Selected Responses: ${this.selectedResponses}`)
  }

  cancel() {
    this.setState({ feedbackText: ''})
    this.setState((prevState) => {
      questions.map((question) => {
        prevState.responses[question.value] = false
      })
    })
  }

  render() {
    const { feedbackText } = this.state
    return (
      <div>
        <div>
          {questions.map(question =>
            (<div
              key={`${question.value}_${question.title}`}
            >
              <label
                htmlFor="answer"
                onClick={() => this.handleCheckboxChange(question.value)}
              >
                {question.title}
                <input
                  type="checkbox"
                  checked={this.state.responses[question.value]}
                  value={question.value}
                  name="answer"
                />
              </label>
            </div>),
          )}
        </div>
        {this.isOtherSelected
          ? <textarea
            placeholder="Type your feedback here..."
            value={feedbackText}
            onChange={(event) => this.handleTextAreaChange(event)}
            name="feedbackTextName"
            data-test-id="textarea"
          />
          : null}
        <div>
          <button onClick={() => this.cancel()}>
            Cancel
          </button>
          <button onClick={() => this.submit()}>
            Submit
          </button>
        </div>
      </div>
    )
  }
}

EsignFeedbackForm.defaultProps = {
  phoneNumber: '1-844-206-0587',
}

EsignFeedbackForm.propTypes = {
  onSubmit: PropTypes.func,
  onCancelClick: PropTypes.func,
  policyNumber: PropTypes.string,
  phoneNumber: PropTypes.string
}

EsignFeedbackForm.defaultProps = {
  onSubmit: () => {},
  onCancelClick: () => {},
  policyNumber: '',
}

export { EsignFeedbackForm }
export default EsignFeedbackForm
