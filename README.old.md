You'll want to run he rollup command and `*` copy it to your ref-arch-2 remote-compnents/compiled dir.

eg: `yarn build; cp build/* ~/projects/ref-arch-2/client/src/remote-components/compiled/`

Things to note.

1. I've only tested with the one component that is exported from this rollup example.
1. I have tested using styled components & a (S)CSS loader but haven't gotten it to work so I wouldn't't bother with that yet or expect it to work.
1. I have only tested a single named export
1. I have yet to try it with a default export (which is kinda important, I got narrowly focused).
1. I commented out the `prop-types` only to make the output cleaner for me to play with. You can put that in again if you need.
1. I don't know if tree shaking works.
1. in the `rollup.config.babel.js` is where we're outputting the bundle.
    * I commented everything out, currently it's only a `UMD` build of a single file. I want to configure that later. It's on the list.
    * It doesn't do any other type of import but will want to in the future. See note above.
    * The babel plugin to run against this export will be part of the build process.

# Requirements
## Manifest (or `package.json`) file
- Determine if this is a private or shared caasette
  - Not sure the reasoning yet outside of a mono repo
- 

## Docs
- Add a docs folder
  - Guide (how to use)
  - API (if required)
  - Recordstore ("Storybook") entry
    - Markdown is huge here
- Should docs be bundled to and sent off "somewhere" to be compiled together?
  - How do we grab and put everything together?
    - Monorepo?
- Each module should be able to have multiple of each of the documentation types
  - Record Store Entry
  - Guide
  - Blog post (is this dumb? I think so but I dunno, leaving it for now anyway)
  - API documentation (code, not web)
  - Change log

## CLI
- Use Plop?
- `eslint` rules
- Dir structure
- Babel plugins
- Package dependancies

## Output
- Can output multiple packages/bundles/modules with:
  - Default exports
  - Named exports
  - Manifest file for each package/bundle/module
  - Hooks up with storybook
    - Eventually use something else
- When the file(s) are finished from `rollup` it should go through the process to become a `caasette`

## Misc
- Move `rollup.config.babel.js` to `./config/rollup.js`
- Remove `./build`
- Remove `./examples`
- Remove `./assets`
  - Image assets not related to packages go into the `./docs` folder
- Add a `./dist` folder.
- Co locate all the tests and remove `./test` folder
- Will I be able to get this to work with `react hooks`? Kinda super dooper important.
- How will we be able to use the main container (caasette-deck) and it's styles within this `record-store` entry? I want to wrap it to make it easier
  - Should be a dev dependancy
- How to version? Is it important to version?
  - Not sure, never been a fan of versioning of the front end
  - This is expecially important for docs
- Extract `Webpack` settings and runner to it's own package
  - Same for anything else that we can to keep things uniform
    - Test runner
    - `Plop` files & scripts
    - `Rollup` config
